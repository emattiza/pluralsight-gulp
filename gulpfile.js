var gulp = require('gulp');
var args = require('yargs').argv;
var browsersync = require('browser-sync');
var config = require('./gulp.config')();
var del = require('del');
var $ = require('gulp-load-plugins')({lazy:true});
var port = process.env.PORT || config.defaultPort;

gulp.task('help', $.taskListing);

gulp.task('default', ['help']);

gulp.task('vet',function() {
    log('Analyzing source with JSHint and JSCS');
    return gulp.src(config.alljs)
    .pipe($.if(args.verbose,$.print()))
    .pipe($.jscs())
    .pipe($.jshint())
    .pipe($.jshint.reporter('jshint-stylish',{verbose:true}))
    .pipe($.jshint.reporter('fail'));
});

gulp.task('styles', ['clean-styles'],function() {
    log('Compiling Less --> CSS');
    return gulp
        .src(config.less)
        .pipe($.plumber())
        .pipe($.less())
        .pipe(gulp.dest(config.temp));
});

gulp.task('fonts',function() {
    return gulp
        .src(config.fonts)
        .pipe(gulp.dest(config.build + 'fonts'));
});

gulp.task('images',function() {
    log('Copying and compressing the images');
    return gulp
        .src(config.images)
        .pipe($.imagemin({optimizationLevel: 4}))
        .pipe(gulp.dest(config.build + 'images'));
});
gulp.task('clean', function(done) {
    var delconfig = [].concat(config.build, config.temp);
    log('Cleaning: ' + $.util.colors.blue(delconfig));
    del(delconfig);
});

gulp.task('clean-fonts',function(done) {
    clean(config.build + 'fonts/**/*.*', done);
});

gulp.task('clean-images',function(done) {
    clean(config.build + 'images/**/*.*', done);
});

gulp.task('clean-code',function(done) {
    var files = [].concat(
        config.temp + '**/*.js',
        config.build + '**/*.html',
        config.build + 'js/**/*.js'
        );
    clean(files,done);
});
gulp.task('clean-styles',function(done) {
    var files = config.temp + '**/*.css';
    clean(files,done);
});

gulp.task('less-watcher', function() {
    gulp.watch([config.less], ['styles']);
});

gulp.task('templatecache', ['clean-code'], function() {
    log('Creating AngularJS $templateCache');
    return gulp
        .src(config.htmltemplates)
        .pipe($.minifyHtml({empty: true}))
        .pipe($.angularTemplatecache(
            config.templateCache.file,
            config.templateCache.options))
        .pipe(gulp.dest(config.temp));
});
gulp.task('wiredep', function() {
    log('Wire up the Bower css js and our app js into html');
    var options = config.getWiredepDefaultOptions();
    var wiredep = require('wiredep').stream;
    return gulp
        .src(config.index)
        .pipe(wiredep(options))
        .pipe($.inject(gulp.src(config.js)))
        .pipe(gulp.dest(config.client));
});

gulp.task('inject',['wiredep','styles'],function() {
    return gulp
        .src(config.index)
        .pipe($.inject(gulp.src(config.css)))
        .pipe(gulp.dest(config.client));
});

gulp.task('optimize',['inject'],function() {
    log('Optimizing the javascript, css, html');
    var templateCache = config.temp + config.templateCache.file;
    return gulp
        .src(config.index)
        .pipe($.plumber())
        .pipe($.inject(gulp.src(templateCache, {read: false}, {
            starttag: '<!-- inject:templates:js -->'
        })))
        .pipe(gulp.dest(config.build));
});

gulp.task('serve-dev',['inject'], function() {
    var isDev = true;
    var nodeOptions = {
        script: config.nodeServer,
        delayTime: 1,
        env: {
            'PORT': port,
            'NODE_ENV': isDev ? 'dev' : 'build'
        },
        watch: [config.server] //TODO define files to restart on
    };
    return $.nodemon(nodeOptions)
        .on('restart', function(ev) {
            log('*** nodemon restarted');
            log('files changed on restart:\n' + ev);
        })
        .on('start',function() {
            log('*** nodemon started');
            startBrowserSync();
        })
        .on('crash',function() {
            log('*** nodemon crash: script crashed for some reason');
        })
        .on('exit',function() {
            log('*** nodemon exited cleanly');
        });
});
//////////////////////////////////
function changeEvent(event) {
    var srcPattern = new RegExp('/.*(?=/' + config.source + ')/');
    log('File ' + event.path.replace(srcPattern,'') + ' ' + event.type);
}
function startBrowserSync() {
    if (browsersync.active) {
        return;
    }
    log('Starting browser-sync on port ' + port);
    gulp.watch([config.less],['styles'])
        .on('change', function(event) { changeEvent(event);});
    var options = {
        proxy: 'localhost:' + port,
        port: 3000,
        files: [config.client + '**/*.*',
                '!' + config.less,
                config.temp + '**/*.css'],
        ghostMode: {
            clicks: true,
            location: false,
            forms: true,
            scroll: true
        },
        injectChanges: true,
        logLevel: 'debug',
        logPrefix: 'gulp-patterns',
        notify: true,
        reloadDelay: 1000
    };
    browsersync(options);
}
function clean(path,done) {
    log('Cleaning: ' + $.util.colors.blue(path));
    del(path,done);
}

function log(msg) {
    if (typeof(msg) === 'object') {
        for (var item in msg) {
            if (msg.hasOwnProperty(item)) {
                $.util.log($.util.colors.blue(msg[item]));
            }
        }
    } else {
        $.util.log($.util.colors.blue(msg));
    }
}
